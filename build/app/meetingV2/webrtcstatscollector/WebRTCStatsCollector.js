"use strict";
// Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var StreamDirection;
(function (StreamDirection) {
    StreamDirection["Downstream"] = "Downstream";
    StreamDirection["Upstream"] = "Upstream";
})(StreamDirection || (StreamDirection = {}));
;
var getCurrentUpstreamMetrics = function (report, timestamp) {
    var currentMetrics = {};
    var frameHeight = report.frameHeight, frameWidth = report.frameWidth, bytesSent = report.bytesSent, packetsSent = report.packetsSent, framesEncoded = report.framesEncoded;
    currentMetrics['frameHeight'] = frameHeight;
    currentMetrics['frameWidth'] = frameWidth;
    currentMetrics['framesEncoded'] = framesEncoded;
    currentMetrics['bytesSent'] = bytesSent;
    currentMetrics['packetsSent'] = packetsSent;
    currentMetrics['timestamp'] = timestamp;
    return currentMetrics;
};
var getCurrentDownstreamMetrics = function (report, timestamp) {
    var currentMetrics = {};
    var bytesReceived = report.bytesReceived, packetsLost = report.packetsLost, packetsReceived = report.packetsReceived, framesDecoded = report.framesDecoded;
    currentMetrics['bytesReceived'] = bytesReceived;
    currentMetrics['packetsLost'] = packetsLost;
    currentMetrics['packetsReceived'] = packetsReceived;
    currentMetrics['framesDecoded'] = framesDecoded;
    var totalPackets = packetsReceived + packetsLost;
    currentMetrics['packetLossPercent'] = totalPackets ? Math.trunc(packetsLost * 100 / (packetsReceived + packetsLost)) : 0;
    currentMetrics['timestamp'] = timestamp;
    return currentMetrics;
};
var bitsPerSecond = function (metricName, metricMap) {
    var previousTimestamp = metricMap.previous.timestamp;
    if (!previousTimestamp) {
        return 0;
    }
    var currentTimestamp = metricMap.current.timestamp;
    var intervalSeconds = (currentTimestamp - previousTimestamp) / 1000;
    if (intervalSeconds <= 0) {
        return 0;
    }
    var diff = (metricMap.current[metricName] - (metricMap.previous[metricName] || 0)) *
        8;
    if (diff <= 0) {
        return 0;
    }
    return Math.trunc((diff / intervalSeconds) / 1000);
};
var countPerSecond = function (metricName, metricMap) {
    var previousTimestamp = metricMap.previous.timestamp;
    if (!previousTimestamp) {
        return 0;
    }
    var currentTimestamp = metricMap.current.timestamp;
    var intervalSeconds = (currentTimestamp - previousTimestamp) / 1000;
    if (intervalSeconds <= 0) {
        return 0;
    }
    var diff = metricMap.current[metricName] - (metricMap.previous[metricName] || 0);
    if (diff <= 0) {
        return 0;
    }
    return Math.trunc(diff / intervalSeconds);
};
var WebRTCStatsCollector = /** @class */ (function () {
    function WebRTCStatsCollector() {
        var _this = this;
        // Map SSRC to WebRTC metrics.
        this.upstreamMetrics = {};
        this.downstreamTileIndexToTrackId = {};
        // Map tile index to SSRC to WebRTC metrics.
        this.downstreamMetrics = {};
        this.cleanUpStaleUpstreamMetricsData = function () {
            var e_1, _a;
            var timestamp = Date.now();
            var ssrcsToRemove = [];
            try {
                for (var _b = __values(Object.keys(_this.upstreamMetrics)), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var ssrc = _c.value;
                    if ((timestamp - _this.upstreamMetrics[ssrc].current.timestamp) >= WebRTCStatsCollector.CLEANUP_INTERVAL) {
                        ssrcsToRemove.push(ssrc);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            ssrcsToRemove.forEach(function (ssrc) { return delete _this.upstreamMetrics[ssrc]; });
        };
        this.processWebRTCStatReportForTileIndex = function (rtcStatsReport, tileIndex) {
            _this.cleanUpStaleUpstreamMetricsData();
            var timestamp = Date.now();
            var ssrcNum = 0;
            rtcStatsReport.forEach(function (report) {
                if (report.ssrc) {
                    ssrcNum = Number(report.ssrc);
                }
                if (report.kind && report.kind === 'video') {
                    if (report.type === 'outbound-rtp' &&
                        report.bytesSent &&
                        report.frameHeight &&
                        report.frameWidth) {
                        // Collect and process upstream stats.
                        if (!_this.upstreamMetrics.hasOwnProperty(ssrcNum)) {
                            if (Object.keys(_this.upstreamMetrics).length === WebRTCStatsCollector.MAX_UPSTREAMS_COUNT) {
                                _this.upstreamMetrics = {};
                            }
                            _this.upstreamMetrics[ssrcNum] = {
                                current: {},
                                previous: {}
                            };
                        }
                        else {
                            _this.upstreamMetrics[ssrcNum].previous = _this.upstreamMetrics[ssrcNum].current;
                        }
                        _this.upstreamMetrics[ssrcNum].current = getCurrentUpstreamMetrics(report, timestamp);
                        _this.upstreamMetrics[ssrcNum].current['framesEncodedPerSecond'] = countPerSecond('framesEncoded', _this.upstreamMetrics[ssrcNum]);
                        _this.upstreamMetrics[ssrcNum].current['bitrate'] = bitsPerSecond('bytesSent', _this.upstreamMetrics[ssrcNum]);
                    }
                    else {
                        if (report.type === 'inbound-rtp' && report.bytesReceived) {
                            // Collect and process downstream stats.
                            var trackId = report.trackId;
                            if (!_this.downstreamMetrics.hasOwnProperty(tileIndex)) {
                                _this.downstreamMetrics[tileIndex] = {};
                            }
                            if (!_this.downstreamMetrics[tileIndex].hasOwnProperty(ssrcNum)) {
                                if (Object.keys(_this.downstreamMetrics[tileIndex]).length === WebRTCStatsCollector.MAX_DOWNSTREAMS_COUNT) {
                                    _this.downstreamMetrics[tileIndex] = {};
                                }
                                _this.downstreamMetrics[tileIndex][ssrcNum] = {
                                    current: {},
                                    previous: {}
                                };
                                // Store trackId to later map frameHeight and frameWidth when WebRTC 'track' report is received for downstream videos.
                                _this.downstreamTileIndexToTrackId[tileIndex] = trackId;
                            }
                            else {
                                _this.downstreamMetrics[tileIndex][ssrcNum].previous = _this.downstreamMetrics[tileIndex][ssrcNum].current;
                            }
                            _this.downstreamMetrics[tileIndex][ssrcNum].current = getCurrentDownstreamMetrics(report, timestamp);
                            _this.downstreamMetrics[tileIndex][ssrcNum].current['bitrate'] = bitsPerSecond('bytesReceived', _this.downstreamMetrics[tileIndex][ssrcNum]);
                            _this.downstreamMetrics[tileIndex][ssrcNum].current['framesDecodedPerSecond'] = countPerSecond('framesDecoded', _this.downstreamMetrics[tileIndex][ssrcNum]);
                        }
                        else if (report.type === 'track' && _this.downstreamTileIndexToTrackId[tileIndex] && _this.downstreamTileIndexToTrackId[tileIndex] === report.id) {
                            // Collect and process frame height and width stats for downstream.
                            // Process frameHeight and frameWidth separately from track report as we do not have these stats in WebRTC 'inbound-rtp' report.
                            var frameHeight = report.frameHeight, frameWidth = report.frameWidth;
                            _this.downstreamMetrics[tileIndex][ssrcNum].current.frameHeight = frameHeight;
                            _this.downstreamMetrics[tileIndex][ssrcNum].current.frameWidth = frameWidth;
                        }
                    }
                }
            });
        };
        this.resetStats = function () {
            _this.upstreamMetrics = {};
            _this.downstreamMetrics = {};
            _this.downstreamTileIndexToTrackId = {};
        };
        this.showStats = function (tileIndex, streamDirection, keyStatstoShow, metricsData) {
            var e_2, _a, e_3, _b, e_4, _c;
            var streams = Object.keys(metricsData);
            if (streams.length === 0) {
                return;
            }
            var statsInfo = document.getElementById("stats-info-" + tileIndex);
            if (!statsInfo) {
                statsInfo = document.createElement('div');
                statsInfo.setAttribute('id', "stats-info-" + tileIndex);
                statsInfo.setAttribute('class', "stats-info");
            }
            var statsInfoTableId = "stats-table-" + tileIndex;
            var statsInfoTable = document.getElementById(statsInfoTableId);
            if (statsInfoTable) {
                statsInfo.removeChild(statsInfoTable);
            }
            statsInfoTable = document.createElement('table');
            statsInfoTable.setAttribute('id', statsInfoTableId);
            statsInfoTable.setAttribute('class', 'stats-table');
            statsInfo.appendChild(statsInfoTable);
            var videoEl = document.getElementById("video-" + tileIndex);
            videoEl.insertAdjacentElement('afterend', statsInfo);
            var header = statsInfoTable.insertRow(-1);
            var cell = header.insertCell(-1);
            cell.innerHTML = 'Video Metrics';
            for (var cnt = 0; cnt < streams.length; cnt++) {
                cell = header.insertCell(-1);
                cell.innerHTML = streamDirection + " " + (cnt + 1);
            }
            try {
                for (var _d = __values(Object.entries(keyStatstoShow)), _e = _d.next(); !_e.done; _e = _d.next()) {
                    var _f = __read(_e.value, 2), key = _f[0], value = _f[1];
                    var row = statsInfoTable.insertRow(-1);
                    row.setAttribute('id', streamDirection + "-" + key + "-" + tileIndex);
                    cell = row.insertCell(-1);
                    cell.innerHTML = value;
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_e && !_e.done && (_a = _d.return)) _a.call(_d);
                }
                finally { if (e_2) throw e_2.error; }
            }
            try {
                for (var streams_1 = __values(streams), streams_1_1 = streams_1.next(); !streams_1_1.done; streams_1_1 = streams_1.next()) {
                    var ssrc = streams_1_1.value;
                    var _g = metricsData[ssrc].current, frameHeight = _g.frameHeight, frameWidth = _g.frameWidth, restStatsToShow = __rest(_g, ["frameHeight", "frameWidth"]);
                    if (frameHeight && frameWidth) {
                        var row = document.getElementById(streamDirection + "-resolution-" + tileIndex);
                        cell = row.insertCell(-1);
                        cell.innerHTML = frameWidth + " &#x2715; " + frameHeight;
                    }
                    try {
                        for (var _h = (e_4 = void 0, __values(Object.entries(restStatsToShow))), _j = _h.next(); !_j.done; _j = _h.next()) {
                            var _k = __read(_j.value, 2), metricName = _k[0], value = _k[1];
                            if (keyStatstoShow[metricName]) {
                                var row = document.getElementById(streamDirection + "-" + metricName + "-" + tileIndex);
                                cell = row.insertCell(-1);
                                cell.innerHTML = "" + value;
                            }
                        }
                    }
                    catch (e_4_1) { e_4 = { error: e_4_1 }; }
                    finally {
                        try {
                            if (_j && !_j.done && (_c = _h.return)) _c.call(_h);
                        }
                        finally { if (e_4) throw e_4.error; }
                    }
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (streams_1_1 && !streams_1_1.done && (_b = streams_1.return)) _b.call(streams_1);
                }
                finally { if (e_3) throw e_3.error; }
            }
        };
    }
    WebRTCStatsCollector.prototype.showUpstreamStats = function (tileIndex) {
        this.showStats(tileIndex, StreamDirection.Upstream, WebRTCStatsCollector.upstreamMetricsKeyStatsToShow, this.upstreamMetrics);
    };
    WebRTCStatsCollector.prototype.showDownstreamStats = function (tileIndex) {
        this.showStats(tileIndex, StreamDirection.Downstream, WebRTCStatsCollector.downstreamMetricsKeyStatsToShow, this.downstreamMetrics[tileIndex]);
    };
    WebRTCStatsCollector.MAX_UPSTREAMS_COUNT = 2;
    WebRTCStatsCollector.MAX_DOWNSTREAMS_COUNT = 1;
    WebRTCStatsCollector.CLEANUP_INTERVAL = 1500; // In milliseconds.
    WebRTCStatsCollector.upstreamMetricsKeyStatsToShow = {
        'resolution': 'Resolution',
        'bitrate': 'Bitrate (kbps)',
        'packetsSent': 'Packets Sent',
        'framesEncodedPerSecond': 'Frame Rate',
    };
    WebRTCStatsCollector.downstreamMetricsKeyStatsToShow = {
        'resolution': 'Resolution',
        'bitrate': 'Bitrate (kbps)',
        'packetLossPercent': 'Packet Loss (%)',
        'framesDecodedPerSecond': 'Frame Rate',
    };
    return WebRTCStatsCollector;
}());
exports.default = WebRTCStatsCollector;
//# sourceMappingURL=WebRTCStatsCollector.js.map