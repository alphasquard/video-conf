"use strict";
// Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * [[Attendee]] contains the information of an attendee.
 */
var Attendee = /** @class */ (function () {
    function Attendee() {
    }
    return Attendee;
}());
exports.default = Attendee;
//# sourceMappingURL=Attendee.js.map