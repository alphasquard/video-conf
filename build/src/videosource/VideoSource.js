"use strict";
// Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * [[VideoSource]] contains the information of a video source.
 */
var VideoSource = /** @class */ (function () {
    function VideoSource() {
    }
    return VideoSource;
}());
exports.default = VideoSource;
//# sourceMappingURL=VideoSource.js.map