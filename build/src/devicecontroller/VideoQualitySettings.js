"use strict";
// Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0
Object.defineProperty(exports, "__esModule", { value: true });
var VideoQualitySettings = /** @class */ (function () {
    function VideoQualitySettings(videoWidth, videoHeight, videoFrameRate, videoMaxBandwidthKbps) {
        this.videoWidth = videoWidth;
        this.videoHeight = videoHeight;
        this.videoFrameRate = videoFrameRate;
        this.videoMaxBandwidthKbps = videoMaxBandwidthKbps;
    }
    return VideoQualitySettings;
}());
exports.default = VideoQualitySettings;
//# sourceMappingURL=VideoQualitySettings.js.map